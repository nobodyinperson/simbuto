% simbuto-graph(1) | simple budgeting tool

NAME
====


**simbuto-graph** - create graph from simbuto budget given on STDIN


SYNOPSIS
========


Usage: /usr/bin/simbuto-graph [options]


Options:
        --opening_stock=OPENING_STOCK


        -o OUTPUT, --output=OUTPUT


        -z ENSEMBLE_SIZE, --ensemble_size=ENSEMBLE_SIZE


        -s START_DATE, --start_date=START_DATE


        -e END_DATE, --end_date=END_DATE


        --width=WIDTH


        --height=HEIGHT


        -h, --help
                Show this help message and exit


AUTHOR
======


Yann Büchau <nobodyinperson@gmx.de>


